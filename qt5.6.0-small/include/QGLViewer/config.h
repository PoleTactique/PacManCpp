/****************************************************************************

 Copyright (C) 2002-2014 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.7.1.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

///////////////////////////////////////////////////////////////////
//               libQGLViewer configuration file                 //
//  Modify these settings according to your local configuration  //
///////////////////////////////////////////////////////////////////

#ifndef QGLVIEWER_CONFIG_H
#define QGLVIEWER_CONFIG_H

#define QGLVIEWER_VERSION 0x020701

// Needed for Qt < 4 (?)
#ifndef QT_CLEAN_NAMESPACE
#define QT_CLEAN_NAMESPACE
#endif

// Get QT_VERSION and other Qt flags
#include <QtCore/QtCore>

#if QT_VERSION < 0x050400
Error : libQGLViewer requires a minimum Qt version of 5.4 Error
        : Use a version prior to 2.7.0 to remove this constraint
#endif

// Win 32 DLL export macros
#ifdef Q_OS_WIN32
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#endif // Q_OS_WIN32

#define QGLVIEWER_EXPORT

// OpenGL includes - Included here and hence shared by all the files that need
// OpenGL headers.
#include <QtOpenGL/QtOpenGL>

// GLU was removed from Qt in version 4.8
#ifdef Q_OS_MAC
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

// For deprecated methods
// #define __WHERE__ "In file "<<__FILE__<<", line "<<__LINE__<<": "
// #define orientationAxisAngle(x,y,z,a) { std::cout << __WHERE__ <<
// "getOrientationAxisAngle()." << std::endl; exit(0); }

// Patch for gcc version <= 2.95. Seems to no longer be needed with recent Qt
// versions. Uncomment these lines if you have error message dealing with
// operator << on QStrings #if defined(__GNUC__) && defined(__GNUC_MINOR__) &&
// (__GNUC__ < 3) && (__GNUC_MINOR__ < 96) # include <iostream> # include
// <qstring.h> std::ostream& operator<<(std::ostream& out, const QString& str)
// { out << str.latin1();  return out; }
// #endif

#endif // QGLVIEWER_CONFIG_H
