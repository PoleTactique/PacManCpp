#ifndef PACMANWINDOW_H
#define PACMANWINDOW_H

#include <QtCore/QtCore>
#include <QtWidgets/QtWidgets>
#include "jeu.h"

class PacmanWindow : public QFrame
{
  protected:
    Jeu jeu;
    QPixmap pixmapPacman, pixmapFantome, pixmapMur;

  public:
    PacmanWindow(QWidget *pParent=0, Qt::WindowFlags flags=0);
    clickMonBoutonajout();
    clickMonBoutonsuppr();
    void handleTimer();

  protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *);
};


class PacmanButton:public QPushButton
{
public:
    PacmanButton(QWidget*parent):QPushButton(parent){};
    PacmanButton(const QString&s,QWidget*p):QPushButton(s,p){};

private:
    void keyPressEvent(QKeyEvent *e);
};

#endif
